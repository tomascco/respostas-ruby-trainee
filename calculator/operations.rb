require_relative '../extra_operations'
require 'net/http'
require 'json'

module Calculator
  class Operations
    include ExtraOperations
  
    def biased_mean(grades, blacklist)
      grades = JSON.parse(grades)
      blacklist = blacklist.split
      
      sum = 0
      n = 0

      grades.each do |student, grade|
        unless blacklist.include? student
          sum += grade
          n += 1
        end
      end
      return n != 0 ? sum.to_f / n : 0 # não dividir por 0

    end
  
    def no_integers(numbers)
      numbers = numbers.split
      answer = ''
      # finais que tornam o número divisível por 25
      divisible = ["00", "25", "50", "75"]
      numbers.each do |number|
        final = number[-2..-1]
        if divisible.include? final
          answer += 'S '
        else 
           += 'N '
        end
      end
      return answer
    end
  
    def filter_films(genres, year)
      films = get_films['movies']
      filter = []
      
      films.each do |film|
        film_genres = film['genres']
        film_year = film['year'].to_i

        # vê se todos os generos do filtro estão presentes no filme
        if genres.all? { |genre| film_genres.include? genre }
          filter << film if film_year >= year
        end
      end
      
      return filter
  
    end
    
    private
  
    def get_films
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/filmes-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response)
    end
  end
end
